import { useDispatch, useSelector } from 'react-redux'
import styles from './Modal.module.scss'
import PropTypes from 'prop-types'
import { closeModal, addToCart, removeFromCart } from '../../redux/actions';


function Modal({text}){

    const dispatch = useDispatch();
    const modal = useSelector((state) => state.modal )
    const closeModalWindow = () => {
        dispatch(closeModal(false));
    }

    const addProductToCart = () => {
        dispatch(addToCart(modal.payload));
        closeModalWindow();
    }

    const removeProduct = () => {
        dispatch(removeFromCart(modal.payload));
        closeModalWindow();
    }


    if (!modal) {
        return null; 
    }

    return (
        <div className={modal? styles.active : styles.hidden} onClick = {closeModalWindow}>
             <div className={styles.modal__content} onClick = { event => event.stopPropagation()}>
                <span className={styles.modal__text}> {text} </span>

                <span className={styles.close} onClick={closeModalWindow}> &times; </span>
                <div className={styles.buttons}>
                    <button className={styles.modal__action_btn} onClick={closeModalWindow}> Скасувати </button>
                    <button className={styles.modal__action_btn} onClick={modal.type==="Видалити" ? removeProduct : addProductToCart}> {modal.type} </button>
                </div>

            </div>
        </div>
)}

Modal.propTypes = {
    text: PropTypes.string.isRequired,
}

export default Modal;