export const UPDATE_PRODUCT_LIST = 'UPDATE_PRODUCT_LIST'
export const ADD_TO_FAVORITES = 'ADD_TO_FAVORITES'
export const REMOVE_FROM_FAVORITES = 'REMOVE_FROM_FAVORITES'
export const ADD_TO_CART = 'ADD_TO_CART'
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART'
export const OPEN_MODAL = 'OPEN_MODAL'
export const CLOSE_MODAL = 'CLOSE_MODAL'
export const CLEAR_CART = 'CLEAR_CART'


export const updateProductList = () => async(dispatch) => {
    const products =
    await fetch('products.json').then((res)=>res.json()).then(data =>
      data.products);

    return dispatch ({
        type: UPDATE_PRODUCT_LIST,
        payload: products
    })
}

export const addToFavorites = (favorites) => {
    return{
        type: ADD_TO_FAVORITES,
        payload: favorites
    }
}

export const removeFromFavorites = (info) => {
    return{
        type: REMOVE_FROM_FAVORITES,
        payload: info

    }
}

export const addToCart = (cart) => {
    return{
        type: ADD_TO_CART,
        payload: cart
    }
}

export const removeFromCart = (info) => {
    return {
        type: REMOVE_FROM_CART,
        payload: info

    }
}

export const openModal = (modal) =>{
    return{
        type: OPEN_MODAL,
        payload: modal
    }
}

export const closeModal = (modal) => {
    return{
        type: CLOSE_MODAL,
        payload: modal
    }
}

export const clearCart = (cart) =>{
    return{
        type: CLEAR_CART,
        payload: cart
    }
}