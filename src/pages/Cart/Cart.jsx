import { useSelector } from "react-redux";
import Modal from "../../components/Modal/Modal";
import Product from "./CartItems";
import styles from "./cart.module.css"
import OrderForm from "../../components/formDelivery/formDelivery";

function Cart() {
    const cart = useSelector((state) => state.cart);

    let sum = 0;
        cart.map(value => {
        sum += value['price'];
        });

    return (
        <>
        <h2 className={styles.title}>Ваша корзина:</h2>
        <h2 className={styles.title}>У вашому кошику є продукти на {sum} грн</h2>
        <div className={styles.wrapper}>
            <div className="products">
                {cart.map(product => <Product key = {product.id} info={product} />)}
            </div>

            <div className={styles.deliveryForm}>
                <h2 className={styles.form_title}>Замовити доставку</h2>
                <OrderForm />
            </div>
        </div>
        
            <Modal text = 'Ви впевнені, що хочете видалити цей товар з корзини?'/>
        </>
     );
}

export default Cart;